var RANGE_ALERT_IN_SEC = 2; //in seconds

var state = 'stop';
var map;
var drawing = false;
var lastLatLng;
var lastTime = 0;
var polylinesA = [];
var polylinesB = [];
var selectedPolyline = "";
var markerA;
var markerB;
var isRun;
var coordNumberA = 0;
var coordNumberB = 0;
var timerA;
var trainId;
var carId;
var timerB;
var timerColiision;
var intersectPoint = null;
var simulationDataA = [];
var simulationDataB = [];

String.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10);
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
}

$(function () {
    initMap();
    getAllNames();

    google.maps.LatLng.prototype.kmTo = function(a){
        var e = Math, ra = e.PI/180;
        var b = this.lat() * ra, c = a.lat() * ra, d = b - c;
        var g = this.lng() * ra - a.lng() * ra;
        var f = 2 * e.asin(e.sqrt(e.pow(e.sin(d/2), 2) + e.cos(b) * e.cos
        (c) * e.pow(e.sin(g/2), 2)));
        return f * 6378.137;
    }

    google.maps.Polyline.prototype.inKm = function(n){
        var a = this.getPath(n);
        return a.getAt(0).kmTo(a.getAt(1));
    }

});

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 53.0640776, lng: 16.729915},
        zoom: 8
    });

    google.maps.event.addListener(map, 'click', function(event) {
        if (drawing) {
            drawing = false;
        }
        else {
            if(selectedPolyline == "A"){
                deletePolyline(polylinesA);
                lastLatLng = event.latLng;
                lastTime = 0;
                addMarker("A");
                setStart(lastLatLng);
                var button = d3.select("#button_play").classed('btn-success', false);
                button.select("i").attr('class', "fa fa-play");
            } else if(selectedPolyline == "B"){
                deletePolyline(polylinesB);
                lastLatLng = event.latLng;
                lastTime = 0;
                addMarker("B");
                setStart(lastLatLng);
                var button = d3.select("#button_play").classed('btn-success', false);
                button.select("i").attr('class', "fa fa-play");
            }
            intersectPoint = null;
            drawing = true;
        }
    });

    google.maps.event.addListener(map, 'mousemove', function(event) {
        if (drawing) {
            if(selectedPolyline == "A"){
                drawPolylineA(event.latLng);
            }
            else if(selectedPolyline == "B"){
                drawPolylineB(event.latLng)
            }
        }
    });
    markerA = new google.maps.Marker();
    markerB = new google.maps.Marker();
    getLevelCrossing();
}

function toLatLng(coord) {
    return new google.maps.LatLng(
        coord.x,
        coord.y
    );
}

function drawOnMap(coordinates) {
    selectedPolyline = "";
    var coordA = coordinates[0].coordinates;
    var coordB = coordinates[1].coordinates;
    var lenA = coordA.length;
    var lenB = coordB.length;

    deletePolyline(polylinesB);
    deletePolyline(polylinesA);

    polylinesA = [];
    polylinesB = [];

    for (i=0; i<lenA; i++){
        if(i == 0){
            lastLatLng = toLatLng(coordA[i]);
            lastTime = 0;

            markerA.setMap(null);
            markerA = new SlidingMarker({
                position: lastLatLng,
                label: "A",
                map: map
            });

        }else {
            drawPolylineA(
                toLatLng(
                    coordA[i]
                ));
        }
    }

    for (i=0; i<lenB; i++){
        if(i == 0){
            lastLatLng = toLatLng(coordB[i]);
            lastTime = 0;

            markerB.setMap(null);
            markerB = new SlidingMarker({
                position: lastLatLng,
                label: "B",
                map: map
            });

        }else {
            drawPolylineB(
                toLatLng(
                    coordB[i]
                ));
        }
    }
}

function drawPolylineA(tolatLng) {
    var polyline = new google.maps.Polyline({
        path: [ lastLatLng, tolatLng ],
        strokeColor: "#ffd655",
        strokeOpacity: 1.0,
        strokeWeight: 5
    });


    polyline.setMap(map);

    polylinesA.push(polyline);

    google.maps.event.addListener(polyline, 'click', function(event) {
        if (drawing) {
            drawing = false;
            selectedPolyline = "";
        }
    });

    google.maps.event.addListener(polyline, 'mouseover', function(event) {
        if(drawing && selectedPolyline == "B" && intersectPoint == null){
            console.log(event.latLng);
            intersectPoint = event.latLng;
        }
    });

    lastLatLng = tolatLng;
}

function drawPolylineB(tolatLng) {
    var polyline = new google.maps.Polyline({
        path: [ lastLatLng, tolatLng ],
        strokeColor: "#fcff19",
        strokeOpacity: 1.0,
        strokeWeight: 5
    });


    polyline.setMap(map);

    polylinesB.push(polyline);

    google.maps.event.addListener(polyline, 'click', function(event)
    {
        if (drawing) {
            drawing = false;
        }
    });

    google.maps.event.addListener(polyline, 'mouseover', function(event) {
        if(drawing && selectedPolyline == "A" && intersectPoint == null){
            console.log(event.latLng);

            intersectPoint = event.latLng;
        }
    });

    lastLatLng = tolatLng;
}

function deletePolyline(polylines) {
    for (i=0; i<polylines.length; i++) {
        polylines[i].setMap(null);
    }
    if(selectedPolyline == "A"){
        polylinesA = [];
    } else if(selectedPolyline == "B"){
        polylinesB = [];
    }
    lastLatLng = null;
}

function addMarker(label) {
    if(selectedPolyline == "A"){
        markerA.setMap(null);
        markerA = new SlidingMarker({
            position: lastLatLng,
            label: label,
            map: map
        });
    }
    else if(selectedPolyline == "B"){
        markerB.setMap(null);
        markerB = new SlidingMarker({
            position: lastLatLng,
            label: label,
            map: map
        });
    }
}

function setDrawingA() {
    selectedPolyline = "A";
    drawing = false;
    lastLatLng = null;
}

function setDrawingB() {
    selectedPolyline = "B";
    drawing = false;
    lastLatLng = null;
}

function deleteDrawing() {
    selectedPolyline = "";
    drawing = false;
    lastLatLng = null;
}

function buttonPlayPress() {
    if(state=='stop'){
        state='play';
        var button = d3.select("#button_play").classed('btn-success', true);
        button.select("i").attr('class', "fa fa-pause");
        isRun = true;

        var velocityA = $("#velocityA").val();
        var velocityB = $("#velocityB").val();

        simulationDataB = [];
        simulationDataA = [];

        prepareSimulation(polylinesA, simulationDataA, velocityA);
        prepareSimulation(polylinesB, simulationDataB, velocityB);

        var simulationATime = 0;
        simulationDataA.forEach(function (val){
            simulationATime += val.simulationTime;
        });
        simulationATime = (simulationATime/1000).toFixed(0);

        var simulationBTime = 0;
        simulationDataB.forEach(function (val){
            simulationBTime += val.simulationTime;
        });
        simulationBTime = (simulationBTime/1000).toFixed(0);

        if(polylinesA.length != 0){
            startAnimateMarkerA();
            showSimulationTime(simulationATime--, "timeA");
            timerA = setInterval(function () {
                if(simulationATime < 0){
                    clearInterval(timerA);
                }
                else if(isRun){
                    showSimulationTime(simulationATime--, "timeA");
                }
            }, 1000);
        }
        if (polylinesB.length != 0){
            startAnimateMarkerB();
            showSimulationTime(simulationBTime--, "timeB");
            timerB = setInterval(function () {
                if(simulationBTime < 0){
                    clearInterval(timerB);
                }
                else if(isRun){
                    showSimulationTime(simulationBTime--, "timeB");
                }
            }, 1000);
        }

        if(intersectPoint != null){
            var timeB = 3600 * distanceToIntersectPoint(polylinesB)/velocityB;
            var timeA = 3600 * distanceToIntersectPoint(polylinesA)/velocityA;
            var diff  = Math.abs(timeB-timeA);

            if(diff < RANGE_ALERT_IN_SEC){
                $("#colision")
                    .addClass("bg-danger")
                    .removeClass("bg-success")
                    .html("Możliwa kolizja obiektów za:" +
                        "<div class=\"font-italic\" id=\"collisionTime\">\n" +
                        "                    00:00:00\n" +
                        "                </div>");
                var time = ((timeB+timeA)/2).toFixed(0);
                showSimulationTime(time--, "collisionTime");

                timerColiision = setInterval(function () {
                    if(time < 0){
                        clearInterval(timerColiision);
                    }
                    else if(isRun){
                        showSimulationTime(time--, "collisionTime");
                    }
                }, 1000);
            } else {
                $("#colision")
                    .addClass("bg-success")
                    .removeClass("bg-danger")
                    .html("Nie dojdzie do kolizji obiektów");
            }
        }
        $("#velocityA").prop('disabled', true);
        $("#velocityB").prop('disabled', true);
    }
    else if(state=='play' || state=='resume'){
        state = 'pause';
        d3.select("#button_play i").attr('class', "fa fa-play");
        isRun = false;
    }
    else if(state=='pause'){
        state = 'resume';
        d3.select("#button_play i").attr('class', "fa fa-pause");
        isRun = true;
        if(polylinesA.length != 0){
            startAnimateMarkerA();
        }
        if (polylinesB.length != 0){
            startAnimateMarkerB();
        }
    }
}

function buttonStopPress(){
    state = 'stop';
    var button = d3.select("#button_play").classed('btn-success', false);
    button.select("i").attr('class', "fa fa-play");
    isRun = false;

    clearInterval(timerA);
    clearInterval(timerB);
    clearInterval(timerColiision);

    $("#velocityA").prop('disabled', false);
    $("#velocityB").prop('disabled', false);

    setStartPosition();
}

function setStartPosition() {
    if(polylinesA.length != 0){
        markerA.setPositionNotAnimated(polylinesA[0].getPath().getAt(1));
        coordNumberA = 0;
    }
    if (polylinesB.length != 0){
        markerB.setPositionNotAnimated(polylinesB[0].getPath().getAt(1));
        coordNumberB = 0;
    }
}

function setStart(latlng) {
    if(selectedPolyline == "A" && polylinesB.length != 0){
        markerA.setPositionNotAnimated(latlng);
        markerB.setPositionNotAnimated(polylinesB[0].getPath().getAt(1));
    } else if (selectedPolyline == "B" && polylinesA.length != 0){
        markerB.setPositionNotAnimated(latlng);
        markerA.setPositionNotAnimated(polylinesA[0].getPath().getAt(1));
    }
    else return;
    coordNumberA = 0;
    coordNumberB = 0;
}

function startAnimateMarkerA() {
    var waitTime = simulationDataA[coordNumberA].waitTime;
    var simulationTime = simulationDataA[coordNumberA].simulationTime;

    markerA.setDuration(simulationTime);
    markerA.setEasing("linear");

    var polylineLength = simulationDataA.length-1;

    setTimeout(function () {
        var latLng = simulationDataA[coordNumberA].latLng.getPath().getAt(1);
        markerA.setPosition(latLng);
        if(coordNumberA < polylineLength && isRun){
            var data = {
                "trainId": trainId,
                "lat":latLng.lat(),
                "lng":latLng.lng(),
                "createdAt": new Date().getTime()
            };
            savePosition(data, 'locations/train');
            coordNumberA++;
            startAnimateMarkerA();
        } else if(coordNumberA >= polylineLength){
            coordNumberA = 0;
        }
        }, waitTime);
}

function showSimulationTime(time, id) {
    $("#"+id).html(time.toFixed(0).toHHMMSS());
}

function startAnimateMarkerB() {
    var waitTime = simulationDataB[coordNumberB].waitTime;
    var simulationTime = simulationDataB[coordNumberB].simulationTime;

    markerB.setDuration(simulationTime);
    markerB.setEasing("linear");

    var polylineLength = simulationDataB.length-1;

    setTimeout(function () {
        var latLng = simulationDataB[coordNumberB].latLng.getPath().getAt(1);
        markerB.setPosition(latLng);
        if(coordNumberB < polylineLength && isRun){
            var data = {
                "carId": carId,
                "lat":latLng.lat(),
                "lng":latLng.lng(),
                "createdAt": new Date().getTime()
            };
            savePosition(data, 'locations/car');
            coordNumberB++;
            startAnimateMarkerB();
        } else if(coordNumberB >= polylineLength){
            coordNumberB = 0;
        }

    }, waitTime)
}

function prepareSimulation(polylineArr, simulation, velocity) {
    var len = polylineArr.length;
    if(len == 0) return;

    trainId = "train" + new Date().getTime();
    carId = "car" + new Date().getTime();
    simulation.push({
        "latLng": polylineArr[0],
        "simulationTime": getTimeBetweenNextPoint(polylineArr[0], velocity),
        "waitTime": 0
    });

    for(i = 1; i<len; i++){
        simulation.push({
            "latLng": polylineArr[i],
            "waitTime": getTimeBetweenNextPoint(polylineArr[i - 1], velocity),
            "simulationTime": getTimeBetweenNextPoint(polylineArr[i], velocity)
        });
    }
    simulation.push({
        "latLng": polylineArr[len-1],
        "waitTime": getTimeBetweenNextPoint(polylineArr[len - 2], velocity),
        "simulationTime": getTimeBetweenNextPoint(polylineArr[len-1], velocity)
    });
}

function getTimeBetweenNextPoint(polyline, velocity){
    var dist = polyline.inKm();

    return (dist/velocity)*3600000; //time in ms
}

function distanceToIntersectPoint(polylineArray) {
    if (polylineArray.length == 0 ) return -1;

    var polylineDistance = 0;
    var distFromPointToIntersect = 100;
    var distance = [];

    polylineArray.forEach(function (polyline) {
       var firstLatLng = polyline.getPath().getAt(0);
       var secondLatLng = polyline.getPath().getAt(1);

       var firstDist = firstLatLng.kmTo(intersectPoint);
       var secondDist = secondLatLng.kmTo(intersectPoint);

        if(firstDist < distFromPointToIntersect){
            distFromPointToIntersect = firstDist;
            distance.push({
                "distFromPointToIntersect": distFromPointToIntersect,
                "polylineDistance": polylineDistance
            });
        }
        if(secondDist < distFromPointToIntersect){
            distFromPointToIntersect = secondDist;
            distance.push({
                "distFromPointToIntersect": distFromPointToIntersect,
                "polylineDistance": polylineDistance + polyline.inKm()
            });
        }
        polylineDistance += polyline.inKm();
    });

    console.log(intersectPoint);
    console.log(distance);
    var dist = distance[ distance.length-1 ];
    var total = dist.distFromPointToIntersect + dist.polylineDistance;
    console.log("distance=  " + total);

    return total;
}

function getApolilineData() {
    var pointList = [];

    var len = polylinesA.length;
    for (i=0; i<len-1; i++){
        var point = polylinesA[i].getPath().getAt(0);
        var latLngArr = [point.lat(), point.lng()];
        pointList.push(latLngArr);
    }
    var point = polylinesA[len-1].getPath().getAt(0);
    var latLngArr = [point.lat(), point.lng()];
    pointList.push(latLngArr);
    return pointList;
}

function getBPolilineData() {
    var pointList = [];

    var len = polylinesB.length;
    for (i=0; i<len-1; i++){
        var point = polylinesB[i].getPath().getAt(0);
        var latLngArr = [point.lat(), point.lng()];
        pointList.push(latLngArr);
    }
    var point = polylinesB[len-1].getPath().getAt(0);
    var latLngArr = [point.lat(), point.lng()];
    pointList.push(latLngArr);
    return pointList;
}

function prepareData(){
    var pointList = [[],[]];
    pointList[0] = getApolilineData();
    pointList[1] = getBPolilineData();

    return pointList
}

function getInputName() {
    var name = $("#shapeName").val();

    if(name != "") return name;
}

function getShapeId() {
    return $("#selectShape").val();
}

function getIntersectPoint() {
    return [intersectPoint.lat(), intersectPoint.lng()];
}

function saveSimulation() {
    var data = {
        "type": "MultiPoint",
        "coordinates": prepareData(),
        "intersectPoint": getIntersectPoint(),
        "name": getInputName()
    };
    console.log(data);
    $.ajax({
        url: 'http://localhost:8080/api/shape',
        dataType: 'JSON',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(data),
        processData: false,
        success: function(){
            getAllNames();
            console.log(getIntersectPoint());
        },
        error: function( err ){
            console.log(err);
        }
    });
}

function savePosition(data, path) {
    $.ajax({
        url: 'http://localhost:8080/' + path,
        dataType: 'JSON',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(data),
        processData: false,
        success: function(){
        },
        error: function( err ){
            console.log(err);
        }
    });
}

function updateSimulation() {
    var data = {
        "type": "MultiPoint",
        "coordinates": prepareData(),
        "intersectPoint": getIntersectPoint(),
        "name": getInputName()
    };
    console.log(data);
    $.ajax({
        url: 'http://localhost:8080/api/shape/' + getShapeId(),
        dataType: 'JSON',
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(data),
        processData: false,
        success: function(){
            getAllNames();
        },
        error: function( err ){
            console.log(err);
        }
    });
}

function deleteSimulation() {
    console.log(getShapeId());
    $.ajax({
        type: "DELETE",
        url:"http://localhost:8080/api/shape/" + getShapeId(),
        success: function () {
            getAllNames();
        }
    });
}

function getSimulation() {
    $.ajax({
        type: "GET",
        url:"http://localhost:8080/api/shape/" + getShapeId(),
        dataType: "json",
        success: function (data) {
            console.log(data);
            intersectPoint = new google.maps.LatLng(
                data.intersectPoint[0],
                data.intersectPoint[1]
            );
            $("#shapeName").val(data.name);
            drawOnMap(data.geoJsonMultiPoint);
        }
    });
}

function getLevelCrossing() {
    $.ajax({
        type: "GET",
        url:"http://localhost:8080/locations/level_crossings",
        dataType: "json",
        success: function (datas) {
            for(var i in datas){
                drawLevelCrossingOnMap(datas[i]);
            }
        }
    });
}

function drawLevelCrossingOnMap(data) {
    new google.maps.Marker({
        position: {lat: data.lat, lng: data.lng},
        map: map,
        label: 'T'
    });
}

function getAllNames() {
    $.ajax({
        type: "GET",
        url:"http://localhost:8080/api/shape/",
        dataType: "json",
        success: function (data) {
            $("#selectShape").html("");
            $("#selectShape").append(new  Option("Wybierz symulacje", "", true, true));
            data.forEach(function (obj) {
                $("#selectShape").append(new Option(obj.name, obj.id, false, false));
            });
        }
    });
}

$(function () {
    $("#getSimulation").click(function () {
        getSimulation();
    });

    $("#saveSimulation").click(function () {
        saveSimulation();
    });


    $("#deleteSimulation").click(function () {
        deleteSimulation();
    });

    $("#buttonDropdown").click(function () {
        document.getElementById("myDropdown").classList.toggle("show");
    });

    $("#buttonDraw").click(function () {
        document.getElementById("myDrawDrop").classList.toggle("show");
    });

    window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
});

function openTab(event, name) {
    event.stopPropagation();
    event.preventDefault();
    if(name == 'simulation'){
        $('#simulation').css('display','block');
        $('#properties').css('display','none');
        $('#btnSim').addClass('active');
        $('#btnProp').removeClass('active');
    }else{
        $('#simulation').css('display','none');
        $('#properties').css('display','block');
        $('#btnSim').removeClass('active');
        $('#btnProp').addClass('active');
    }
}

function saveB() {
    var polilineB = getBPolilineData();
    var data = "{\"data\": [";
    for(i in polilineB){
        data += '{"lat":' + polilineB[i][0] + ',"lng":' + polilineB[i][1]  + '},'
    }
    data = data.substring(0, data.length-1);
    data += "]}";
    download(data.toString(), 'Auto_' + new Date().getTime() + '.json', 'json');
}

function download(data, filename, type) {
    var file = new Blob([data], {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        var a = document.createElement("a"),
            url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function () {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 0);
    }
}

function saveA() {
    var polilineA = getApolilineData();
    var data = "{\"data\": [";
    for(i in polilineA){
        data += '{"lat":' + polilineA[i][0] + ',"lng":' + polilineA[i][1]  + '},'
    }
    data = data.substring(0, data.length-1);
    data += "]}";
    download(data.toString(), 'Train_' + new Date().getTime() + '.json', 'json');
}