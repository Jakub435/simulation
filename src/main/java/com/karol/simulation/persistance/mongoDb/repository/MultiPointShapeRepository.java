package com.karol.simulation.persistance.mongoDb.repository;

import com.karol.simulation.persistance.mongoDb.domain.MultiPointShape;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RestResource(exported = false)
public interface MultiPointShapeRepository extends MongoRepository<MultiPointShape, String> {

    @Query(value = "{}", fields = "{id:1, name:1}")
    List<MultiPointShape> getAllNames();
}

