package com.karol.simulation.persistance.mongoDb.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.karol.simulation.customException.CoordinateArrayException;
import com.karol.simulation.customException.Minimum2PointsException;
import org.bson.codecs.pojo.annotations.BsonId;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonMultiPoint;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "shape")
public class MultiPointShape {
    @BsonId
    private String id;
    private String name;
    private List<GeoJsonMultiPoint> geoJsonMultiPoint;
    private List<Double> intersectPoint;

    @JsonCreator
    public MultiPointShape(
            @JsonProperty(value = "coordinates", required = true) JsonNode node
    ) throws Minimum2PointsException {
        List<GeoJsonMultiPoint> geoJsonMultiPointList = new ArrayList<>();

        try {
            for(int i = 0; i<2; i++){
                List<Point> pointList = new ArrayList<>();

                for (JsonNode coordinate:node.get(i)) {
                    Point point = new Point(
                            coordinate.get(0).doubleValue(),
                            coordinate.get(1).doubleValue());

                    pointList.add(point);
                }
                geoJsonMultiPointList.add(new GeoJsonMultiPoint(pointList));
            }

            this.geoJsonMultiPoint = geoJsonMultiPointList;
        }catch (IllegalArgumentException exception){
            exception.printStackTrace();
            throw new Minimum2PointsException();
        }catch (NullPointerException ex){
            ex.printStackTrace();
            throw new CoordinateArrayException();
        }

    }

    public MultiPointShape(List<GeoJsonMultiPoint> geoJsonMultiPoint, String name) {
        this.geoJsonMultiPoint = geoJsonMultiPoint;
        this.name = name;
    }

    public MultiPointShape() {
    }

    public List<Double> getIntersectPoint() {
        return intersectPoint;
    }

    public void setIntersectPoint(List<Double> intersectPoint) {
        this.intersectPoint = intersectPoint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<GeoJsonMultiPoint> getGeoJsonMultiPoint() {
        return geoJsonMultiPoint;
    }

    public void setGeoJsonMultiPoint(List<GeoJsonMultiPoint> geoJsonMultiPoint) {
        this.geoJsonMultiPoint = geoJsonMultiPoint;
    }
}
