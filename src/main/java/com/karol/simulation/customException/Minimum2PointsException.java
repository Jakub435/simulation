package com.karol.simulation.customException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Minumum of 2 Points required")
public class Minimum2PointsException extends RuntimeException{
}