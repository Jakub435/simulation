package com.karol.simulation.router;

import com.karol.simulation.customException.NotFoundException;
import com.karol.simulation.persistance.mongoDb.domain.MultiPointShape;
import com.karol.simulation.service.MultiPointShapeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping(path = "/api/shape")
public class MultiPointShapeController {
    @Autowired
    private MultiPointShapeService multiPointShapeService;

    @PostMapping
    public @ResponseBody
    MultiPointShape saveMultiPointShape(
            @RequestBody MultiPointShape multiPointShape){

        return multiPointShapeService.saveMultiPointShape(multiPointShape);
    }

    @GetMapping(path = "/{shapeId}", produces = "application/json")
    public @ResponseBody
    MultiPointShape getMultiPointShapeById(
            @PathVariable String shapeId) throws NotFoundException {

        return multiPointShapeService.getMultiPointShapeById(shapeId);
    }

    @GetMapping
    public @ResponseBody
    List<MultiPointShape> getAllShapeName(){
        return multiPointShapeService.getAllNames();
    }

    @PutMapping(path = "/{shapeId}", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    MultiPointShape updateMultiPointShapeById(
            @PathVariable String shapeId,
            @RequestBody MultiPointShape multiPointShape) throws NotFoundException {

        return multiPointShapeService
                .updateMultiPointShapeById(shapeId, multiPointShape.getGeoJsonMultiPoint());
    }

    @DeleteMapping(path = "/{shapeId}")
    public @ResponseBody
    ResponseEntity<String> deleteMultiPointShape(
            @PathVariable String shapeId) throws NotFoundException{
        multiPointShapeService.deleteMultiPointShape(shapeId);

        return new ResponseEntity<String>(HttpStatus.OK);
    }
}
